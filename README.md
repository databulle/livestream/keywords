# Keywords

Cleaning and analysing a list of keywords.  

Code from Twitch live, replay (in French) available here:  
- https://www.youtube.com/watch?v=KKZlSxnGr_8  

## Setup

Clone this repository:  

    git clone https://gitlab.com/databulle/livestream/keywords.git  
    cd keywords  

Create virtual environment.  
I recommend using Python 3.11.2:  

    pyenv install 3.11.2
    pyenv virtualenv 3.11.2 keywords
    pyenv local keywords

Install requirements:  

    pip install -r requirements.txt  

All set!  

## Usage

Launch Jupyter local server:  

    jupyter notebook  

Open http://localhost:8888 in your browser.  

